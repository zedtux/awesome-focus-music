# Contribution Guidelines

Please ensure your pull request adheres to the following guidelines:

- Make an individual pull request for each suggestion.
- Use the following format: `link (music length) tags`
- One tag required: The source like #youtube for YouTube and so on
- Link additions should be added to the bottom of the relevant category.
- New categories or improvements to the existing categorization are welcome.
- Make sure your text editor is set to remove trailing whitespace.
- The pull request and commit should have a useful title.

Thank you for your suggestions!
