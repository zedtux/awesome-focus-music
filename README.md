# Awesome Focus Music

The aim of this repository is to build a list of links to musics helping you to
focus on your developments for long hours.

### Alpha waves

 - https://www.youtube.com/watch?v=WPni755-Krg (3 hours) #youtube
 - https://www.youtube.com/watch?v=86_Q7JD58a8 (3 hours) #youtube
 - https://www.youtube.com/watch?v=UkM-FjfN6Mc (2 hours) #youtube #hz
 - https://www.youtube.com/watch?v=wiSUU3G3fGc (8 hours) #youtube #hz

### Indian

 - https://www.youtube.com/watch?v=3ctJKsc7hsg (1 hour) #youtube
 - https://www.youtube.com/watch?v=ouAkkv00bfo (1 hour) #youtube
 - https://www.youtube.com/watch?v=6079Cf16nNE (1 hour) #youtube

### Nature

 - https://www.youtube.com/watch?v=cDISjp3aOEo (42 minutes) #youtube
 - https://www.youtube.com/watch?v=taJ7Rb3qgi8 (2 hours) #youtube

### Shamanic

 - https://www.youtube.com/watch?v=Pj4q0Mc08EY (2 hours) #youtube
 - https://www.youtube.com/watch?v=Tbj3ihvYEso (2 hours) #youtube

### Tibetan

 - https://www.youtube.com/watch?v=vCuyKwFVkMY (6 hours) #youtube
 - https://www.youtube.com/watch?v=Qo7XwpUvsvE (6 hours) #youtube
 - https://www.youtube.com/watch?v=PszSx4HvKAA (6 hours) #youtube
